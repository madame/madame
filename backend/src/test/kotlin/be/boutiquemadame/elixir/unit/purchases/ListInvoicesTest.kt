package be.boutiquemadame.elixir.unit.purchases

import be.boutiquemadame.elixir.purchases.adapters.datastores.inmemory.InvoiceInMemoryDatastore
import be.boutiquemadame.elixir.purchases.domain.InvoiceId
import be.boutiquemadame.elixir.purchases.domain.InvoiceMetadata
import be.boutiquemadame.elixir.purchases.usecases.ListInvoiceMetadataQuery
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.collections.shouldHaveSize
import java.math.BigDecimal
import java.time.LocalDate
import java.util.UUID

class ListInvoicesTest : BehaviorSpec({

    val invoiceMetadataGateway = InvoiceInMemoryDatastore()
    val listInvoicesUseCase = ListInvoiceMetadataQuery(invoiceMetadataGateway)

    Given("I want to list invoices") {

        And("There are no invoices") {

            When("I retrieve all the invoices") {

                Then("I should get an empty list") {

                    val list = listInvoicesUseCase.execute(Unit)
                    list shouldHaveSize 0
                }
            }
        }

        And("There are invoices") {

            val invoiceIds = mutableListOf<InvoiceId>()

            for (i in 1..5) {
                val invoiceMetadata = InvoiceMetadata(
                    id = InvoiceId(UUID.fromString("3dd6b1a5-5169-4cbb-928e-e59dd619efd$i")),
                    internalReference = "Invoice $i",
                    externalReference = "EXT/${100 + 3 * i}",
                    invoiceDate = LocalDate.of(1980 + 10 * i, i, 20 - i),
                    amount = BigDecimal(10 * (3 * i % 5))
                )

                invoiceIds.add(invoiceMetadata.id)
                invoiceMetadataGateway.addMetadata(invoiceMetadata)
            }

            When("I retrieve all the invoices") {

                Then("I should get all of them") {

                    val list = listInvoicesUseCase.execute(Unit)
                    list shouldHaveSize 5
                }
            }
        }
    }
})
