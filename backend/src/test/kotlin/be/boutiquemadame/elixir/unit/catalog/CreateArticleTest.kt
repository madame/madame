package be.boutiquemadame.elixir.unit.catalog

import be.boutiquemadame.elixir.catalog.DuplicatedArticle
import be.boutiquemadame.elixir.catalog.adapters.datastores.inmemory.ArticleInMemoryDatastore
import be.boutiquemadame.elixir.catalog.usecases.CreateArticleCommand
import be.boutiquemadame.elixir.catalog.usecases.CreateArticleRequest
import be.boutiquemadame.elixir.shared.valueobject.Article
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe

class CreateArticleTest : BehaviorSpec({

    val productDatastore = ArticleInMemoryDatastore()
    val createProductUseCase = CreateArticleCommand(productDatastore)

    Given("No products exist") {

        When("I want a create a product") {

            And("This product is an article") {
                val article = Article(
                    model = "Shirt",
                    articleNumber = "184GSD",
                    color = "Blue",
                    size = "XL",
                )

                suspend fun createArticle() {
                    createProductUseCase.execute(
                        CreateArticleRequest(
                            article
                        )
                    )
                }

                And("The article does not exist") {

                    And("The article is created") {

                        createArticle()

                        Then("The product should be saved") {
                            productDatastore.exists(article) shouldBe true
                        }
                    }
                }

                And("The article attributes are already used") {
                    productDatastore.add(article)

                    val exception = shouldThrow<DuplicatedArticle> {
                        createArticle()
                    }

                    Then("The article is correct") {
                        exception.article shouldBe article
                    }
                }
            }
        }
    }
})
