package be.boutiquemadame.elixir.unit.purchases

import be.boutiquemadame.elixir.purchases.ArticleNotFound
import be.boutiquemadame.elixir.purchases.ArticleWithNegativePrice
import be.boutiquemadame.elixir.purchases.DuplicatedArticleIdInInvoice
import be.boutiquemadame.elixir.purchases.DuplicatedInvoiceId
import be.boutiquemadame.elixir.purchases.DuplicatedInvoiceInternalReference
import be.boutiquemadame.elixir.purchases.adapters.datastores.inmemory.ArticleInMemoryDatastore
import be.boutiquemadame.elixir.purchases.adapters.datastores.inmemory.InvoiceInMemoryDatastore
import be.boutiquemadame.elixir.purchases.domain.Invoice
import be.boutiquemadame.elixir.purchases.domain.InvoiceId
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceCommand
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequest
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequestLine
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequestLineWithArticle
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequestTextLine
import be.boutiquemadame.elixir.shared.valueobject.Article
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import java.math.BigDecimal
import java.time.LocalDate
import java.util.UUID

class CreateInvoiceTest : BehaviorSpec({

    val invoiceDatastore = InvoiceInMemoryDatastore()
    val articleDatastore = ArticleInMemoryDatastore()
    val createInvoiceUseCase = CreateInvoiceCommand(invoiceDatastore, articleDatastore)

    Given("I want to create an invoice") {

        var id = UUID.fromString("a1132f72-d5fe-46ca-81d8-437c54a1ae43")
        val invoiceId = InvoiceId(id)
        val internalReference = "Internal"
        val externalReference = "External"
        val invoiceDate = LocalDate.of(2010, 1, 1)
        val invoiceVatAmount = BigDecimal("21.57")
        val invoiceLines: MutableList<CreateInvoiceRequestLine> = mutableListOf()

        suspend fun createInvoice() {
            val createInvoiceRequest = CreateInvoiceRequest(
                id = id,
                internalReference = internalReference,
                externalReference = externalReference,
                invoiceDate = invoiceDate,
                vatAmount = invoiceVatAmount,
                lines = invoiceLines
            )
            return createInvoiceUseCase.execute(createInvoiceRequest)
        }

        suspend fun getSavedInvoice(): Invoice {
            return invoiceDatastore.findById(invoiceId)!!
        }

        And("The invoice has no lines") {

            When("I save it") {
                createInvoice()

                Then("An invoice should have been saved") {
                    invoiceDatastore.findById(invoiceId) shouldNotBe null
                }

                val savedInvoice = getSavedInvoice()

                Then("The invoice date should be saved") {
                    savedInvoice.invoiceDate shouldBe invoiceDate
                }

                Then("The internal reference should be saved should 0") {
                    savedInvoice.internalReference shouldBe internalReference
                }

                Then("The VAT amount should be saved") {
                    savedInvoice.vatAmount shouldBe invoiceVatAmount
                }

                Then("The excluding VAT amount should 0") {
                    savedInvoice.amountExcludingVat() shouldBe BigDecimal(0)
                }

                Then("The total amount should be equals to the VAT amount") {
                    savedInvoice.totalAmount() shouldBe invoiceVatAmount
                }
            }

            And("The id is already used") {
                createInvoice()

                When("I save it") {
                    val exception = shouldThrow<DuplicatedInvoiceId> {
                        createInvoice()
                    }

                    Then("A duplicated invoiceId error should be raised") {
                        exception.invoiceId shouldBe invoiceId
                    }
                }
            }

            And("The internal reference is already used") {
                createInvoice()

                id = UUID.fromString("a1132f72-d5fe-46ca-81d8-437c54a1ae44")

                When("I save it") {

                    val exception = shouldThrow<DuplicatedInvoiceInternalReference> {
                        createInvoice()
                    }

                    Then("A duplicated internal reference error should be raised") {
                        exception.internalReference shouldBe internalReference
                    }
                }
            }
        }

        And("The invoice has a single text line") {
            val lineDescription = "Lorem ipsum"
            val linePrice = BigDecimal("12.5")

            fun addLine() {
                invoiceLines += CreateInvoiceRequestTextLine(lineDescription, linePrice)
            }

            When("The invoice is saved") {
                addLine()
                createInvoice()
                val savedInvoice = getSavedInvoice()
                val savedLines = savedInvoice.lines

                Then("The invoice should have exactly one line") {
                    savedLines shouldHaveSize 1
                }

                val savedFirstLine = savedLines.getOrNull(0)!!

                Then("The line description should be saved") {
                    savedFirstLine.description shouldBe lineDescription
                }

                Then("The line price should be saved") {
                    savedFirstLine.totalPrice shouldBe linePrice
                }

                Then("The line unit price should be saved") {
                    savedFirstLine.unitPrice shouldBe linePrice
                }

                Then("The line unit quantity should be saved") {
                    savedFirstLine.quantity shouldBe 1
                }

                Then("The invoice excluding vat amount should be the line price") {
                    savedInvoice.amountExcludingVat() shouldBe linePrice
                }

                Then("The invoice total amount should be the line price + the VAT") {
                    savedInvoice.totalAmount() shouldBe linePrice + invoiceVatAmount
                }
            }
        }

        And("The invoice has a single line with an article") {
            val model = "Shirt"
            val articleNumber = "487V12"
            val color = "Blue"
            val size = "52"
            val lineQuantity = 2
            var lineUnitPrice = BigDecimal("12.5")

            val article = Article(
                model = model,
                articleNumber = articleNumber,
                color = color,
                size = size,
            )

            fun addLine() {
                invoiceLines += CreateInvoiceRequestLineWithArticle(
                    article = article,
                    quantity = lineQuantity,
                    unitPrice = lineUnitPrice
                )
            }

            And("The article exists") {

                articleDatastore.add(Article(model, articleNumber, color, size))

                When("The invoice is saved") {
                    addLine()
                    createInvoice()
                    val savedInvoice = getSavedInvoice()

                    Then("The invoice lines should not be null") {
                        savedInvoice.lines shouldNotBe null
                    }

                    val savedLines = savedInvoice.lines

                    Then("The invoice should have one line") {
                        savedLines shouldHaveSize 1
                    }

                    val savedFirstLine = savedLines.getOrNull(0)!!

                    Then("The line description should be saved") {
                        savedFirstLine.description shouldBe "$model $articleNumber $color $size"
                    }

                    Then("The line quantity should be saved") {
                        savedFirstLine.quantity shouldBe lineQuantity
                    }

                    Then("The line unit price should be saved") {
                        savedFirstLine.unitPrice shouldBe lineUnitPrice
                    }

                    Then("The line total price should be saved") {
                        savedFirstLine.totalPrice shouldBe BigDecimal(lineQuantity) * lineUnitPrice
                    }

                    Then("The invoice excluding-vat amount should be correct") {
                        savedInvoice.amountExcludingVat() shouldBe lineUnitPrice * BigDecimal(lineQuantity)
                    }
                }

                And("The unit price is negative") {
                    lineUnitPrice = BigDecimal(-12)

                    When("The invoice is saved") {
                        addLine()
                        val exception = shouldThrow<ArticleWithNegativePrice> {
                            createInvoice()
                        }

                        Then("An negative price on article exception should be raised with correct price") {
                            exception.price shouldBe lineUnitPrice
                        }

                        Then("An negative price on article exception should be raised with correct article") {
                            exception.article shouldBe Article(model, articleNumber, color, size)
                        }
                    }
                }
            }

            And("The article does not exists") {

                When("The invoice is saved") {
                    addLine()
                    val exception = shouldThrow<ArticleNotFound> {
                        createInvoice()
                    }

                    Then("An article not found exception should be raised") {
                        exception.article shouldBe Article(model, articleNumber, color, size)
                    }
                }
            }
        }

        And("The invoice has multiple lines") {

            And("When there are no duplicates") {
                for (i in 0..2) {
                    invoiceLines += CreateInvoiceRequestTextLine(
                        description = "Item $i",
                        price = BigDecimal(i + 1)
                    )
                }
                for (i in 0..2) {

                    val model = "T-shirt $i"
                    val articleNumber = "184/$i"
                    val color = "Red"
                    val size = "4$i"

                    val article = Article(
                        model = model,
                        articleNumber = articleNumber,
                        color = color,
                        size = size,
                    )

                    articleDatastore.add(article)

                    invoiceLines += CreateInvoiceRequestLineWithArticle(
                        article = article,
                        quantity = i + 3,
                        unitPrice = BigDecimal(i + 1)
                    )
                }

                When("The invoice is created") {
                    createInvoice()

                    val savedInvoice = getSavedInvoice()

                    Then("Amount with vat should be correct") {
                        savedInvoice.totalAmount() shouldBe BigDecimal("53.57")
                    }
                }
            }

            And("When there are duplicates") {

                val model = "T-shirt"
                val articleNumber = "187"
                val color = "Red"
                val size = "36"

                val article = Article(
                    model = model,
                    articleNumber = articleNumber,
                    color = color,
                    size = size,
                )

                articleDatastore.add(article)

                for (i in 0..1) {
                    invoiceLines += CreateInvoiceRequestLineWithArticle(
                        article = article,
                        quantity = i,
                        unitPrice = BigDecimal(i + 1)
                    )
                }

                When("The invoice is saved") {
                    val exception = shouldThrow<DuplicatedArticleIdInInvoice> {
                        createInvoice()
                    }

                    Then("A duplicated article id  exception should be raised") {
                        exception.article shouldBe Article(model, articleNumber, color, size)
                    }
                }
            }
        }
    }
})
