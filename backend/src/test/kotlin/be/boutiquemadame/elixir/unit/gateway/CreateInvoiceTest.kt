package be.boutiquemadame.elixir.unit.gateway

import be.boutiquemadame.elixir.catalog.usecases.CreateArticleRequest
import be.boutiquemadame.elixir.gateway.adapters.contexts.inmemory.InMemoryPurchasesContextGateway
import be.boutiquemadame.elixir.gateway.adapters.contexts.inmemory.InMemoryWarehouseContextGateway
import be.boutiquemadame.elixir.gateway.usecases.ArticleLineStatus
import be.boutiquemadame.elixir.gateway.usecases.CreateInvoiceCommand
import be.boutiquemadame.elixir.gateway.usecases.CreateInvoiceGatewayArticleLine
import be.boutiquemadame.elixir.gateway.usecases.CreateInvoiceGatewayLine
import be.boutiquemadame.elixir.gateway.usecases.CreateInvoiceGatewayRequest
import be.boutiquemadame.elixir.gateway.usecases.CreateInvoiceGatewayTextLine
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequestLineWithArticle
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequestTextLine
import be.boutiquemadame.elixir.shared.valueobject.Article
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.collections.shouldHaveSize
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import java.math.BigDecimal
import java.time.LocalDate
import java.util.UUID

class CreateInvoiceTest : BehaviorSpec({

    val purchasesContextGateway = InMemoryPurchasesContextGateway()
    val warehouseContextGateway = InMemoryWarehouseContextGateway()

    val createInvoiceUseCase = CreateInvoiceCommand(purchasesContextGateway, warehouseContextGateway)

    Given("No articles or invoices exist") {

        val id = UUID.fromString("5e80fd66-d6e8-4598-9c99-b6b08ab5b744")
        val vatAmount = BigDecimal("12.5")
        val internalReference = "int"
        val externalReference = "ext"
        val invoiceDate = LocalDate.parse("2020-07-20")

        val lines = mutableListOf<CreateInvoiceGatewayLine>()

        suspend fun createInvoice() {
            createInvoiceUseCase.execute(
                CreateInvoiceGatewayRequest(
                    id = id,
                    vatAmount = vatAmount,
                    internalReference = internalReference,
                    externalReference = externalReference,
                    invoiceDate = invoiceDate,
                    lines = lines
                )
            )
        }

        When("I try to create an empty invoice") {

            createInvoice()

            Then("The request should be forwarded to purchases module") {
                purchasesContextGateway.invoiceCreated shouldHaveSize 1
            }

            val requestSent = purchasesContextGateway.invoiceCreated[0]

            Then("The invoice id should be the same") {
                requestSent.id shouldBe id
            }

            Then("The invoice vat amount should be the same") {
                requestSent.vatAmount shouldBe vatAmount
            }

            Then("The invoice internal reference should be the same") {
                requestSent.internalReference shouldBe internalReference
            }

            Then("The invoice external reference should be the same") {
                requestSent.externalReference shouldBe externalReference
            }

            Then("The invoice date should be the same") {
                requestSent.invoiceDate shouldBe invoiceDate
            }
        }

        When("I try to create an invoice with a text line") {

            val description = "Line text"
            val price = BigDecimal("1845.25")

            lines.add(
                CreateInvoiceGatewayTextLine(
                    description = description,
                    price = price
                )
            )

            createInvoice()

            val invoiceCreated = purchasesContextGateway.invoiceCreated[0]

            val line = invoiceCreated.lines[0] as CreateInvoiceRequestTextLine

            Then("The request line should be forwarded with the description") {
                line.description shouldBe description
            }

            Then("The request line should be forwarded with the price") {
                line.price shouldBe price
            }
        }

        When("I try to create an invoice with a line referencing an existing article") {
            val model = "Shirt"
            val articleNumber = "123/458"
            val color = "Red"
            val size = "XS"
            val unitPrice = BigDecimal("18.45")
            val quantity = 2

            val article = Article(
                model = model,
                articleNumber = articleNumber,
                color = color,
                size = size,
            )

            lines.add(
                CreateInvoiceGatewayArticleLine(
                    status = ArticleLineStatus.EXISTING,
                    article = article,
                    unitPrice = unitPrice,
                    quantity = quantity
                )
            )

            createInvoice()

            val invoiceCreated = purchasesContextGateway.invoiceCreated[0]

            val line = invoiceCreated.lines[0] as CreateInvoiceRequestLineWithArticle

            Then("The request line should be forwarded with the article id") {
                line.article shouldBe article
            }

            Then("The request line should be forwarded with the quantity") {
                line.quantity shouldBe quantity
            }

            Then("The request line should be forwarded with the unit price") {
                line.unitPrice shouldBe unitPrice
            }
        }

        When("I try to create an invoice with a line referencing a new article") {

            val model = "T-Shirt"
            val articleNumber = "1845/75"
            val color = "485"
            val size = "36"
            val unitPrice = BigDecimal("18.45")
            val quantity = 2

            val article = Article(
                model = model,
                articleNumber = articleNumber,
                color = color,
                size = size,
            )

            lines.add(
                CreateInvoiceGatewayArticleLine(
                    status = ArticleLineStatus.NEW,
                    article = article,
                    unitPrice = unitPrice,
                    quantity = quantity
                )
            )

            createInvoice()

            val invoiceCreated = purchasesContextGateway.invoiceCreated[0]

            val line = invoiceCreated.lines[0] as CreateInvoiceRequestLineWithArticle

            Then("The request line should be forwarded with the quantity") {
                line.quantity shouldBe quantity
            }

            Then("The request line should be forwarded with the unit price") {
                line.unitPrice shouldBe unitPrice
            }

            val products = warehouseContextGateway.articlesCreated

            Then("A product should have been created") {
                products shouldHaveSize 1
            }

            val product = products[0]

            Then("The product should be an article") {
                product.shouldBeInstanceOf<CreateArticleRequest>()
            }

            Then("The article model should be forwarded") {
                product.article.model shouldBe model
            }

            Then("The article article number should be forwarded") {
                product.article.articleNumber shouldBe articleNumber
            }

            Then("The article color should be forwarded") {
                product.article.color shouldBe color
            }

            Then("The article size should be forwarded") {
                product.article.size shouldBe size
            }
        }
    }
})
