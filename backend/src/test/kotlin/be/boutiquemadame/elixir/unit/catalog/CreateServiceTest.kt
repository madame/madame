package be.boutiquemadame.elixir.unit.catalog

import be.boutiquemadame.elixir.catalog.adapters.datastores.inmemory.ServiceInMemoryDatastore
import be.boutiquemadame.elixir.catalog.domain.ServiceId
import be.boutiquemadame.elixir.catalog.usecases.CreateServiceCommand
import be.boutiquemadame.elixir.catalog.usecases.CreateServiceRequest
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.shouldNotBe
import java.util.UUID

class CreateServiceTest : BehaviorSpec({

    val serviceDatastore = ServiceInMemoryDatastore()
    val createProductUseCase = CreateServiceCommand(serviceDatastore)

    Given("No products exist") {

        When("I want a create a product") {

            val id = UUID.fromString("03baa262-1650-4c5c-b16c-abf17caee427")

            And("This product is a service") {
                val description = "Service 1"

                createProductUseCase.execute(
                    CreateServiceRequest(
                        id,
                        description
                    )
                )

                val service = serviceDatastore.findById(ServiceId(id))

                Then("The product should be saved") {
                    service shouldNotBe null
                }

                Then("The service should have the correct id") {
                    service?.id shouldBe ServiceId(id)
                }

                Then("The service should have the correct description") {
                    service?.description shouldBe description
                }
            }
        }
    }
})
