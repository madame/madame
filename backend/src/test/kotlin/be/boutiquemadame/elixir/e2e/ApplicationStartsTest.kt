package be.boutiquemadame.elixir.e2e

import be.boutiquemadame.elixir.main.main
import io.kotest.core.spec.style.FunSpec
import io.kotest.matchers.shouldBe
import io.kotest.matchers.types.shouldBeInstanceOf
import kotlinx.coroutines.delay
import kotlinx.serialization.ExperimentalSerializationApi
import java.io.IOException
import java.net.Socket
import kotlin.concurrent.thread

@ExperimentalSerializationApi
class ApplicationStartsTest : FunSpec({
    test("Application should start in 1 second") {
        val mainThread = thread {
            main()
        }

        mainThread.setUncaughtExceptionHandler { _, throwable ->
            throwable.shouldBeInstanceOf<InterruptedException>()
        }

        delay(1000)

        isPortListening(8082) shouldBe true

        mainThread.interrupt()
        mainThread.join()
    }
})

private fun isPortListening(port: Int): Boolean {
    return try {
        Socket("localhost", port)
        true
    } catch (e: IOException) {
        false
    }
}
