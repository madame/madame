package be.boutiquemadame.elixir.purchases.domain

import java.math.BigDecimal
import java.time.LocalDate

data class InvoiceMetadata(
    val id: InvoiceId,
    val internalReference: String,
    val externalReference: String,
    val invoiceDate: LocalDate,
    val amount: BigDecimal,
)
