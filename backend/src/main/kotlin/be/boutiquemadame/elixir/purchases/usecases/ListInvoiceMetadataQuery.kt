package be.boutiquemadame.elixir.purchases.usecases

import be.boutiquemadame.elixir.purchases.usecases.contracts.InvoiceDatastore
import be.boutiquemadame.elixir.shared.UseCase
import java.math.BigDecimal
import java.time.LocalDate
import java.util.UUID

class ListInvoiceMetadataQuery(private val invoiceDatastore: InvoiceDatastore) : UseCase<Unit, ListInvoicesResponse> {
    override suspend fun execute(request: Unit): ListInvoicesResponse {
        return invoiceDatastore.findMetadata().map { invoice ->
            ListInvoicesInvoiceMetadata(
                id = invoice.id.raw,
                internalReference = invoice.internalReference,
                externalReference = invoice.externalReference,
                invoiceDate = invoice.invoiceDate,
                amount = invoice.amount,
            )
        }
    }
}

typealias ListInvoicesResponse = List<ListInvoicesInvoiceMetadata>

data class ListInvoicesInvoiceMetadata(
    val id: UUID,
    val internalReference: String,
    val externalReference: String,
    val invoiceDate: LocalDate,
    val amount: BigDecimal,
)
