package be.boutiquemadame.elixir.purchases.domain

import be.boutiquemadame.elixir.purchases.DuplicatedArticleIdInInvoice
import be.boutiquemadame.elixir.shared.sum
import be.boutiquemadame.elixir.shared.valueobject.Article
import java.math.BigDecimal
import java.time.LocalDate

data class Invoice(
    val id: InvoiceId,
    val internalReference: String,
    val externalReference: String,
    val invoiceDate: LocalDate,
    val lines: List<InvoiceLine>,
    val vatAmount: BigDecimal,
) {

    fun amountExcludingVat(): BigDecimal {
        return lines
            .map { it.totalPrice }
            .sum()
    }

    fun totalAmount(): BigDecimal {
        return amountExcludingVat() + vatAmount
    }

    init {
        validateDuplicatedArticle()
    }

    private fun validateDuplicatedArticle() {
        val duplicate = findDuplicate()

        if (duplicate != null) {
            throw DuplicatedArticleIdInInvoice(duplicate)
        }
    }

    private fun findDuplicate(): Article? {
        return lines
            .filterIsInstance<ArticleInvoiceLine>()
            .groupingBy { it.article }
            .eachCount()
            .filter { it.value > 1 }
            .keys
            .firstOrNull()
    }
}
