package be.boutiquemadame.elixir.purchases.adapters.datastores.inmemory

import be.boutiquemadame.elixir.purchases.usecases.contracts.ArticleDatastore
import be.boutiquemadame.elixir.shared.valueobject.Article

class ArticleInMemoryDatastore : ArticleDatastore {
    private val articleIds = HashSet<Article>()

    override suspend fun exists(article: Article): Boolean {
        return articleIds.contains(article)
    }

    fun add(article: Article) {
        articleIds.add(article)
    }
}
