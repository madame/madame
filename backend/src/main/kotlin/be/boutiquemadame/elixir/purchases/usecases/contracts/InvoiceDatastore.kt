package be.boutiquemadame.elixir.purchases.usecases.contracts

import be.boutiquemadame.elixir.purchases.domain.Invoice
import be.boutiquemadame.elixir.purchases.domain.InvoiceId
import be.boutiquemadame.elixir.purchases.domain.InvoiceMetadata

interface InvoiceDatastore {
    suspend fun findById(id: InvoiceId): Invoice?
    suspend fun internalReferenceExists(internalReference: String): Boolean
    suspend fun save(invoice: Invoice)
    suspend fun findMetadata(): List<InvoiceMetadata>
}
