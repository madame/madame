package be.boutiquemadame.elixir.purchases.domain

import java.util.UUID

data class InvoiceId(val raw: UUID)
