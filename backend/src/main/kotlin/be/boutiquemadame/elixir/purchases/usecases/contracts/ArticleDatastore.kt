package be.boutiquemadame.elixir.purchases.usecases.contracts

import be.boutiquemadame.elixir.shared.valueobject.Article

interface ArticleDatastore {
    suspend fun exists(article: Article): Boolean
}
