package be.boutiquemadame.elixir.purchases.usecases

import be.boutiquemadame.elixir.purchases.ArticleNotFound
import be.boutiquemadame.elixir.purchases.DuplicatedInvoiceId
import be.boutiquemadame.elixir.purchases.DuplicatedInvoiceInternalReference
import be.boutiquemadame.elixir.purchases.domain.ArticleInvoiceLine
import be.boutiquemadame.elixir.purchases.domain.Invoice
import be.boutiquemadame.elixir.purchases.domain.InvoiceId
import be.boutiquemadame.elixir.purchases.domain.InvoiceLine
import be.boutiquemadame.elixir.purchases.domain.TextInvoiceLine
import be.boutiquemadame.elixir.purchases.usecases.contracts.ArticleDatastore
import be.boutiquemadame.elixir.purchases.usecases.contracts.InvoiceDatastore
import be.boutiquemadame.elixir.shared.Command
import be.boutiquemadame.elixir.shared.valueobject.Article
import java.math.BigDecimal
import java.time.LocalDate
import java.util.UUID

class CreateInvoiceCommand(
    private val invoiceGateway: InvoiceDatastore,
    private val articleGateway: ArticleDatastore
) : Command<CreateInvoiceRequest> {

    override suspend fun execute(request: CreateInvoiceRequest) {
        validate(request)

        val invoice = buildInvoice(request)

        save(invoice)
    }

    private suspend fun validate(request: CreateInvoiceRequest) {
        validateNotUsed(request.id)
        validateInternalReferenceNotUsed(request.internalReference)
        validateArticlesExist(request.lines)
    }

    private suspend fun validateNotUsed(id: UUID) {
        val invoiceId = InvoiceId(id)
        if (invoiceGateway.findById(invoiceId) != null) {
            throw DuplicatedInvoiceId(invoiceId)
        }
    }

    private suspend fun validateInternalReferenceNotUsed(externalReference: String) {
        if (invoiceGateway.internalReferenceExists(externalReference)) {
            throw DuplicatedInvoiceInternalReference(externalReference)
        }
    }

    private suspend fun validateArticlesExist(lines: List<CreateInvoiceRequestLine>) {
        lines
            .filterIsInstance<CreateInvoiceRequestLineWithArticle>()
            .forEach { line ->
                val article = line.article
                if (!articleGateway.exists(article)) {
                    throw ArticleNotFound(article)
                }
            }
    }

    private fun buildInvoice(request: CreateInvoiceRequest): Invoice {
        val invoiceId = request.id
        val lines = request.lines.map {
            buildLine(it)
        }

        return Invoice(
            id = InvoiceId(invoiceId),
            internalReference = request.internalReference,
            externalReference = request.externalReference,
            invoiceDate = request.invoiceDate,
            lines = lines,
            vatAmount = request.vatAmount,
        )
    }

    private suspend fun save(invoice: Invoice) {
        invoiceGateway.save(
            invoice
        )
    }

    private fun buildLine(line: CreateInvoiceRequestLine): InvoiceLine {
        return when (line) {
            is CreateInvoiceRequestTextLine -> {
                TextInvoiceLine(line.description, line.price)
            }
            is CreateInvoiceRequestLineWithArticle -> {
                ArticleInvoiceLine(line.article, line.quantity, line.unitPrice)
            }
        }
    }
}

data class CreateInvoiceRequest(
    val id: UUID,
    val internalReference: String,
    val externalReference: String,
    val invoiceDate: LocalDate,
    val vatAmount: BigDecimal,
    val lines: List<CreateInvoiceRequestLine>
)

sealed class CreateInvoiceRequestLine

data class CreateInvoiceRequestTextLine(
    val description: String,
    val price: BigDecimal
) : CreateInvoiceRequestLine()

data class CreateInvoiceRequestLineWithArticle(
    val article: Article,
    val quantity: Int,
    val unitPrice: BigDecimal
) : CreateInvoiceRequestLine()
