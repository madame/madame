package be.boutiquemadame.elixir.purchases.domain

import be.boutiquemadame.elixir.purchases.ArticleWithNegativePrice
import be.boutiquemadame.elixir.shared.valueobject.Article
import java.math.BigDecimal

sealed class InvoiceLine {
    abstract val description: String
    abstract val unitPrice: BigDecimal
    abstract val quantity: Int
    abstract val totalPrice: BigDecimal
}

data class TextInvoiceLine(override val description: String, val price: BigDecimal) : InvoiceLine() {

    override val unitPrice = price

    override val quantity = 1

    override val totalPrice = price
}

data class ArticleInvoiceLine(val article: Article, override val quantity: Int, override val unitPrice: BigDecimal) : InvoiceLine() {
    init {
        if (unitPrice < BigDecimal.ZERO) {
            throw ArticleWithNegativePrice(article, unitPrice)
        }
    }

    override val description = article.description

    override val totalPrice = BigDecimal(quantity) * unitPrice
}
