package be.boutiquemadame.elixir.purchases

import be.boutiquemadame.elixir.purchases.domain.InvoiceId
import be.boutiquemadame.elixir.shared.valueobject.Article
import java.math.BigDecimal

data class ArticleNotFound(val article: Article) : Exception("Article $article not found or is not an article")
data class ArticleWithNegativePrice(val article: Article, val price: BigDecimal) : Exception("Article $article has a negative price $price")
data class DuplicatedArticleIdInInvoice(val article: Article) : Exception("Article $article is duplicated")
data class DuplicatedInvoiceId(val invoiceId: InvoiceId) : java.lang.Exception("Invoice ${invoiceId.raw} already exists")
data class DuplicatedInvoiceInternalReference(val internalReference: String) : java.lang.Exception("Invoice reference $internalReference is already used")
