package be.boutiquemadame.elixir.purchases.adapters.datastores.inmemory

import be.boutiquemadame.elixir.purchases.domain.Invoice
import be.boutiquemadame.elixir.purchases.domain.InvoiceId
import be.boutiquemadame.elixir.purchases.domain.InvoiceMetadata
import be.boutiquemadame.elixir.purchases.usecases.contracts.InvoiceDatastore

class InvoiceInMemoryDatastore : InvoiceDatastore {
    private val invoices = HashMap<InvoiceId, Invoice>()
    private val invoiceMetadata = HashMap<InvoiceId, InvoiceMetadata>()

    override suspend fun findById(id: InvoiceId): Invoice? {
        return invoices[id]
    }

    override suspend fun internalReferenceExists(internalReference: String): Boolean {
        return invoices
            .values
            .any { invoice -> invoice.internalReference == internalReference }
    }

    override suspend fun save(invoice: Invoice) {
        invoices[invoice.id] = invoice

        invoiceMetadata[invoice.id] =
            InvoiceMetadata(
                id = invoice.id,
                internalReference = invoice.internalReference,
                externalReference = invoice.externalReference,
                invoiceDate = invoice.invoiceDate,
                amount = invoice.totalAmount()
            )
    }

    override suspend fun findMetadata(): List<InvoiceMetadata> {
        return invoiceMetadata.values.toList()
    }

    fun addMetadata(invoiceMetadata: InvoiceMetadata) {
        this.invoiceMetadata[invoiceMetadata.id] = invoiceMetadata
    }
}
