package be.boutiquemadame.elixir.purchases.adapters.datastores.real

import be.boutiquemadame.elixir.purchases.usecases.contracts.ArticleDatastore
import be.boutiquemadame.elixir.shared.valueobject.Article

class ArticleIdWarehouseContextDatastore(
    private val articleDatastore: be.boutiquemadame.elixir.catalog.usecases.contracts.ArticleDatastore
) : ArticleDatastore {
    override suspend fun exists(article: Article): Boolean {
        return articleDatastore.exists(article)
    }
}
