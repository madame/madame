package be.boutiquemadame.elixir.catalog.adapters.datastores.inmemory

import be.boutiquemadame.elixir.catalog.usecases.contracts.ArticleDatastore
import be.boutiquemadame.elixir.shared.valueobject.Article

class ArticleInMemoryDatastore : ArticleDatastore {
    private val articles = HashSet<Article>()

    override suspend fun exists(article: Article): Boolean {
        return articles.contains(article)
    }

    override suspend fun add(article: Article) {
        articles.add(article)
    }
}
