package be.boutiquemadame.elixir.catalog.domain

import java.util.UUID

data class ServiceId(val raw: UUID)
