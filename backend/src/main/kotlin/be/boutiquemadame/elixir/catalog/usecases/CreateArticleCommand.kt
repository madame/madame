package be.boutiquemadame.elixir.catalog.usecases

import be.boutiquemadame.elixir.catalog.DuplicatedArticle
import be.boutiquemadame.elixir.catalog.usecases.contracts.ArticleDatastore
import be.boutiquemadame.elixir.shared.Command
import be.boutiquemadame.elixir.shared.valueobject.Article

class CreateArticleCommand(private val articleDatastore: ArticleDatastore) : Command<CreateArticleRequest> {
    override suspend fun execute(request: CreateArticleRequest) {

        val article = request.article

        if (articleDatastore.exists(article)) {
            throw DuplicatedArticle(article)
        }

        articleDatastore.add(
            article
        )
    }
}

data class CreateArticleRequest(val article: Article)
