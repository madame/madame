package be.boutiquemadame.elixir.catalog.usecases.contracts

import be.boutiquemadame.elixir.shared.valueobject.Article

interface ArticleDatastore {

    suspend fun exists(article: Article): Boolean
    suspend fun add(article: Article)
}
