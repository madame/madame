package be.boutiquemadame.elixir.catalog

import be.boutiquemadame.elixir.catalog.domain.ServiceId
import be.boutiquemadame.elixir.shared.valueobject.Article

data class DuplicatedServiceId(val serviceId: ServiceId) : Exception("Service ${serviceId.raw} already exists")
data class DuplicatedArticle(val article: Article) : Exception("Article ${article.description} already exists")
