package be.boutiquemadame.elixir.catalog.adapters.datastores.inmemory

import be.boutiquemadame.elixir.catalog.domain.Service
import be.boutiquemadame.elixir.catalog.domain.ServiceId
import be.boutiquemadame.elixir.catalog.usecases.contracts.ServiceDatastore

class ServiceInMemoryDatastore : ServiceDatastore {
    private val products = HashMap<ServiceId, Service>()

    override suspend fun findById(id: ServiceId): Service? {
        return products[id]
    }

    override suspend fun save(service: Service) {
        products[service.id] = service
    }
}
