package be.boutiquemadame.elixir.catalog.usecases.contracts

import be.boutiquemadame.elixir.catalog.domain.Service
import be.boutiquemadame.elixir.catalog.domain.ServiceId

interface ServiceDatastore {

    suspend fun findById(id: ServiceId): Service?
    suspend fun save(service: Service)
}
