package be.boutiquemadame.elixir.catalog.usecases

import be.boutiquemadame.elixir.catalog.DuplicatedServiceId
import be.boutiquemadame.elixir.catalog.domain.Service
import be.boutiquemadame.elixir.catalog.domain.ServiceId
import be.boutiquemadame.elixir.catalog.usecases.contracts.ServiceDatastore
import be.boutiquemadame.elixir.shared.Command
import java.util.UUID

class CreateServiceCommand(private val serviceDatastore: ServiceDatastore) : Command<CreateServiceRequest> {
    override suspend fun execute(request: CreateServiceRequest) {
        val productId = ServiceId(request.id)
        if (serviceDatastore.findById(productId) != null) {
            throw DuplicatedServiceId(productId)
        }

        val product =
            Service(
                productId,
                request.description
            )

        serviceDatastore.save(
            product
        )
    }
}

data class CreateServiceRequest(val id: UUID, val description: String)
