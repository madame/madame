package be.boutiquemadame.elixir.catalog.domain

data class Service(
    val id: ServiceId,
    val description: String
)
