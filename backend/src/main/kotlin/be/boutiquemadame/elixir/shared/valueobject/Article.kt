package be.boutiquemadame.elixir.shared.valueobject

data class Article(
    val model: String,
    val articleNumber: String,
    val color: String,
    val size: String
) {
    val description: String = "$model $articleNumber $color $size"
}
