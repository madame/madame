package be.boutiquemadame.elixir.main

import io.ktor.http.HttpStatusCode
import java.lang.Exception

sealed class HttpException(val errorCode: HttpStatusCode, override val message: String?) : Exception(message)
class BadRequest(override val message: String?) : HttpException(HttpStatusCode.BadRequest, message)
class Conflict(override val message: String?) : HttpException(HttpStatusCode.Conflict, message)
