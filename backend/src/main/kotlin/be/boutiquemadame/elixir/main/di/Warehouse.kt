package be.boutiquemadame.elixir.main.di

import be.boutiquemadame.elixir.catalog.adapters.datastores.inmemory.ArticleInMemoryDatastore
import be.boutiquemadame.elixir.catalog.usecases.CreateArticleCommand
import be.boutiquemadame.elixir.catalog.usecases.contracts.ArticleDatastore
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.instance
import org.kodein.di.singleton

val warehouseModule = DI.Module("Warehouse") {

    bind<ArticleDatastore>() with singleton { ArticleInMemoryDatastore() }
    bind() from singleton { CreateArticleCommand(instance()) }
}
