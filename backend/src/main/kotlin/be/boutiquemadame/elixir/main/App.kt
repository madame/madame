package be.boutiquemadame.elixir.main

import be.boutiquemadame.elixir.gateway.adapters.primary.http.controllers.InvoiceController
import be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers
import be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers.CreateInvoiceHttpRequest
import be.boutiquemadame.elixir.main.di.gatewayModule
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.AutoHeadResponse
import io.ktor.features.CORS
import io.ktor.features.CallLogging
import io.ktor.features.Compression
import io.ktor.features.ContentNegotiation
import io.ktor.features.HSTS
import io.ktor.features.StatusPages
import io.ktor.http.ContentType
import io.ktor.http.HttpMethod
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.serialization.json
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.SerializationException
import kotlinx.serialization.json.Json
import org.kodein.di.instance
import org.kodein.di.ktor.di

@ExperimentalSerializationApi
fun Application.app() {
    install(CallLogging)
    install(Compression)
    // install(HttpsRedirect)
    install(HSTS)
    install(CORS) {
        anyHost()
        header("Origin")
        method(HttpMethod.Options)
        allowNonSimpleContentTypes = true
    }
    // install(ForwardedHeaderSupport)
    install(AutoHeadResponse)
    install(ContentNegotiation) {
        json(
            contentType = ContentType.Any,
            json = Json {
                serializersModule = serializers
                prettyPrint = true
            }
        )
    }
    install(StatusPages) {
        exception<HttpException> { e ->
            call.respond(e.errorCode, e.message.orEmpty())
        }
        exception<SerializationException> { e ->
            e.printStackTrace()
            call.respond(HttpStatusCode.BadRequest, "Serialization error")
        }
        exception<Throwable> { e ->
            e.printStackTrace()
            call.respond(HttpStatusCode.InternalServerError)
        }
    }
    /*install(DIFeature) {

    }*/

    di {
        import(gatewayModule)
    }

    routing {
        val controller by di().instance<InvoiceController>()

        post("/invoices") {
            val request = call.receive<CreateInvoiceHttpRequest>()
            controller.create(request)
            call.respond(HttpStatusCode.NoContent)
        }

        get("/invoices") {
            val response = controller.getAll()
            call.respond(response)
        }
    }
}
