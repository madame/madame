package be.boutiquemadame.elixir.main

import io.ktor.application.Application
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.serialization.ExperimentalSerializationApi

@ExperimentalSerializationApi
fun main() {
    embeddedServer(
        Netty,
        port = 8082,
        module = Application::app
    ).start(true)
}
