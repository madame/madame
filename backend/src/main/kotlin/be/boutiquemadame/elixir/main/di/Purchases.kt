package be.boutiquemadame.elixir.main.di

import be.boutiquemadame.elixir.purchases.adapters.datastores.inmemory.InvoiceInMemoryDatastore
import be.boutiquemadame.elixir.purchases.adapters.datastores.real.ArticleIdWarehouseContextDatastore
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceCommand
import be.boutiquemadame.elixir.purchases.usecases.ListInvoiceMetadataQuery
import be.boutiquemadame.elixir.purchases.usecases.contracts.ArticleDatastore
import be.boutiquemadame.elixir.purchases.usecases.contracts.InvoiceDatastore
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.instance
import org.kodein.di.singleton

val purchaseModule = DI.Module("Purchases") {
    bind<InvoiceDatastore>() with singleton { InvoiceInMemoryDatastore() }
    bind<ArticleDatastore>() with singleton { ArticleIdWarehouseContextDatastore(instance()) }
    bind() from singleton { CreateInvoiceCommand(instance(), instance()) }
    bind() from singleton { ListInvoiceMetadataQuery(instance()) }
}
