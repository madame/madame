package be.boutiquemadame.elixir.main.di

import be.boutiquemadame.elixir.gateway.adapters.contexts.real.RealPurchasesContextGateway
import be.boutiquemadame.elixir.gateway.adapters.contexts.real.RealWarehouseContextGateway
import be.boutiquemadame.elixir.gateway.adapters.primary.http.controllers.InvoiceController
import be.boutiquemadame.elixir.gateway.usecases.CreateInvoiceCommand
import be.boutiquemadame.elixir.gateway.usecases.contracts.PurchasesContextGateway
import be.boutiquemadame.elixir.gateway.usecases.contracts.WarehouseContextGateway
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.instance
import org.kodein.di.singleton

val gatewayModule = DI.Module("Gateway") {

    importOnce(purchaseModule)
    importOnce(warehouseModule)

    bind<PurchasesContextGateway>() with singleton { RealPurchasesContextGateway(instance()) }
    bind<WarehouseContextGateway>() with singleton { RealWarehouseContextGateway(instance()) }
    bind() from singleton { CreateInvoiceCommand(instance(), instance()) }
    bind() from singleton { InvoiceController(instance(), instance()) }
}
