package be.boutiquemadame.elixir.gateway.usecases.contracts

import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequest

interface PurchasesContextGateway {

    suspend fun createInvoice(request: CreateInvoiceRequest)
}
