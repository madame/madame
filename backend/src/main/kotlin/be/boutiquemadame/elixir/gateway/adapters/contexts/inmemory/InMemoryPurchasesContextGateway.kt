package be.boutiquemadame.elixir.gateway.adapters.contexts.inmemory

import be.boutiquemadame.elixir.gateway.usecases.contracts.PurchasesContextGateway
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequest

class InMemoryPurchasesContextGateway : PurchasesContextGateway {
    val invoiceCreated = mutableListOf<CreateInvoiceRequest>()

    override suspend fun createInvoice(request: CreateInvoiceRequest) {
        invoiceCreated.add(request)
    }
}
