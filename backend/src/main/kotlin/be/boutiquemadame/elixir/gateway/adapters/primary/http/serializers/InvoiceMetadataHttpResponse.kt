package be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import java.util.UUID

@Serializable
data class InvoiceMetadataHttpResponse(
    @Contextual val id: UUID,
    val internalReference: String,
    val externalReference: String,
    @Contextual val invoiceDate: LocalDate,
    @Contextual val amount: BigDecimal
)
