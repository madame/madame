package be.boutiquemadame.elixir.gateway.adapters.primary.http

import be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers.BigDecimalSerializer
import be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers.LocalDateSerializer
import be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers.UUIDSerializer
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.modules.SerializersModule
import kotlinx.serialization.modules.contextual

@ExperimentalSerializationApi
val serializers = SerializersModule {
    contextual(UUIDSerializer)
    contextual(LocalDateSerializer)
    contextual(BigDecimalSerializer)
}
