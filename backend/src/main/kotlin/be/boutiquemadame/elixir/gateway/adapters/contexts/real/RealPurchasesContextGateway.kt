package be.boutiquemadame.elixir.gateway.adapters.contexts.real

import be.boutiquemadame.elixir.gateway.usecases.contracts.PurchasesContextGateway
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceCommand
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequest

class RealPurchasesContextGateway(
    val createInvoiceCommand: CreateInvoiceCommand
) : PurchasesContextGateway {
    override suspend fun createInvoice(request: CreateInvoiceRequest) {
        createInvoiceCommand.execute(request)
    }
}
