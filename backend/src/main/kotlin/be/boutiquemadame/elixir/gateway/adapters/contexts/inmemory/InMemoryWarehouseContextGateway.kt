package be.boutiquemadame.elixir.gateway.adapters.contexts.inmemory

import be.boutiquemadame.elixir.catalog.usecases.CreateArticleRequest
import be.boutiquemadame.elixir.gateway.usecases.contracts.WarehouseContextGateway

class InMemoryWarehouseContextGateway : WarehouseContextGateway {
    val articlesCreated = mutableListOf<CreateArticleRequest>()

    override suspend fun createArticle(request: CreateArticleRequest) {
        articlesCreated.add(request)
    }
}
