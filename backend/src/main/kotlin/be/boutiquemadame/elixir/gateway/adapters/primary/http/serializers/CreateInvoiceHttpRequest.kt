package be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers

import kotlinx.serialization.Contextual
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import java.math.BigDecimal
import java.time.LocalDate
import java.util.UUID

@Serializable
data class CreateInvoiceHttpRequest(
    @Contextual val id: UUID,
    val internalReference: String,
    val externalReference: String,
    @Contextual val invoiceDate: LocalDate,
    @Contextual val vatAmount: BigDecimal,
    val lines: List<CreateInvoiceHttpRequestLine>
)

@Serializable
sealed class CreateInvoiceHttpRequestLine

@Serializable
@SerialName("text")
data class CreateInvoiceHttpRequestTextLine(
    val description: String,
    @Contextual val price: BigDecimal
) : CreateInvoiceHttpRequestLine()

@Serializable
@SerialName("existing-article")
data class CreateInvoiceHttpRequestExistingArticleLine(
    val model: String,
    val articleNumber: String,
    val color: String,
    val size: String,
    @Contextual val unitPrice: BigDecimal,
    val quantity: Int,
) : CreateInvoiceHttpRequestLine()

@Serializable
@SerialName("new-article")
data class CreateInvoiceHttpRequestNewArticleLine(
    val model: String,
    val articleNumber: String,
    val color: String,
    val size: String,
    @Contextual val unitPrice: BigDecimal,
    val quantity: Int,
) : CreateInvoiceHttpRequestLine()
