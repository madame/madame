package be.boutiquemadame.elixir.gateway.usecases

import be.boutiquemadame.elixir.catalog.usecases.CreateArticleRequest
import be.boutiquemadame.elixir.gateway.usecases.contracts.PurchasesContextGateway
import be.boutiquemadame.elixir.gateway.usecases.contracts.WarehouseContextGateway
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequest
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequestLineWithArticle
import be.boutiquemadame.elixir.purchases.usecases.CreateInvoiceRequestTextLine
import be.boutiquemadame.elixir.shared.Command
import be.boutiquemadame.elixir.shared.valueobject.Article
import java.math.BigDecimal
import java.time.LocalDate
import java.util.UUID

class CreateInvoiceCommand(
    private val purchasesContextGateway: PurchasesContextGateway,
    private val warehouseContextGateway: WarehouseContextGateway,
) : Command<CreateInvoiceGatewayRequest> {

    override suspend fun execute(request: CreateInvoiceGatewayRequest) {

        val lines = request.lines.map { line ->
            when (line) {
                is CreateInvoiceGatewayTextLine -> {
                    CreateInvoiceRequestTextLine(
                        description = line.description,
                        price = line.price
                    )
                }

                is CreateInvoiceGatewayArticleLine -> {
                    if (line.status === ArticleLineStatus.NEW) {
                        warehouseContextGateway.createArticle(
                            CreateArticleRequest(
                                line.article
                            )
                        )
                    }

                    CreateInvoiceRequestLineWithArticle(
                        article = line.article,
                        quantity = line.quantity,
                        unitPrice = line.unitPrice
                    )
                }
            }
        }

        purchasesContextGateway.createInvoice(
            CreateInvoiceRequest(
                id = request.id,
                internalReference = request.internalReference,
                externalReference = request.externalReference,
                vatAmount = request.vatAmount,
                invoiceDate = request.invoiceDate,
                lines = lines
            )
        )
    }
}

data class CreateInvoiceGatewayRequest(
    val id: UUID,
    val invoiceDate: LocalDate,
    val internalReference: String,
    val externalReference: String,
    val vatAmount: BigDecimal,
    val lines: List<CreateInvoiceGatewayLine>
)

sealed class CreateInvoiceGatewayLine

data class CreateInvoiceGatewayTextLine(
    val description: String,
    val price: BigDecimal,
) : CreateInvoiceGatewayLine()

data class CreateInvoiceGatewayArticleLine(
    val status: ArticleLineStatus,
    val article: Article,
    val quantity: Int,
    val unitPrice: BigDecimal
) : CreateInvoiceGatewayLine()

enum class ArticleLineStatus {
    NEW,
    EXISTING
}
