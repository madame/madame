package be.boutiquemadame.elixir.gateway.usecases.contracts

import be.boutiquemadame.elixir.catalog.usecases.CreateArticleRequest

interface WarehouseContextGateway {

    suspend fun createArticle(request: CreateArticleRequest)
}
