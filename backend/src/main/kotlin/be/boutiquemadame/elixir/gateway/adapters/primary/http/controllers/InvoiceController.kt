package be.boutiquemadame.elixir.gateway.adapters.primary.http.controllers

import be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers.CreateInvoiceHttpRequest
import be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers.CreateInvoiceHttpRequestExistingArticleLine
import be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers.CreateInvoiceHttpRequestNewArticleLine
import be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers.CreateInvoiceHttpRequestTextLine
import be.boutiquemadame.elixir.gateway.adapters.primary.http.serializers.InvoiceMetadataHttpResponse
import be.boutiquemadame.elixir.gateway.usecases.ArticleLineStatus
import be.boutiquemadame.elixir.gateway.usecases.CreateInvoiceCommand
import be.boutiquemadame.elixir.gateway.usecases.CreateInvoiceGatewayArticleLine
import be.boutiquemadame.elixir.gateway.usecases.CreateInvoiceGatewayRequest
import be.boutiquemadame.elixir.gateway.usecases.CreateInvoiceGatewayTextLine
import be.boutiquemadame.elixir.main.BadRequest
import be.boutiquemadame.elixir.main.Conflict
import be.boutiquemadame.elixir.purchases.ArticleNotFound
import be.boutiquemadame.elixir.purchases.ArticleWithNegativePrice
import be.boutiquemadame.elixir.purchases.DuplicatedArticleIdInInvoice
import be.boutiquemadame.elixir.purchases.DuplicatedInvoiceId
import be.boutiquemadame.elixir.purchases.usecases.ListInvoiceMetadataQuery
import be.boutiquemadame.elixir.shared.valueobject.Article

class InvoiceController(
    private val createInvoiceCommand: CreateInvoiceCommand,
    private val listInvoicesQuery: ListInvoiceMetadataQuery
) {

    suspend fun create(request: CreateInvoiceHttpRequest) {
        try {

            createInvoiceCommand.execute(
                CreateInvoiceGatewayRequest(
                    id = request.id,
                    internalReference = request.internalReference,
                    externalReference = request.externalReference,
                    vatAmount = request.vatAmount,
                    invoiceDate = request.invoiceDate,
                    lines = request.lines.map { line ->
                        when (line) {
                            is CreateInvoiceHttpRequestTextLine -> {
                                CreateInvoiceGatewayTextLine(
                                    description = line.description,
                                    price = line.price
                                )
                            }
                            is CreateInvoiceHttpRequestExistingArticleLine -> {
                                CreateInvoiceGatewayArticleLine(
                                    status = ArticleLineStatus.EXISTING,
                                    article = Article(
                                        model = line.model,
                                        articleNumber = line.articleNumber,
                                        color = line.color,
                                        size = line.size,
                                    ),
                                    unitPrice = line.unitPrice,
                                    quantity = line.quantity,
                                )
                            }
                            is CreateInvoiceHttpRequestNewArticleLine -> {
                                CreateInvoiceGatewayArticleLine(
                                    status = ArticleLineStatus.NEW,
                                    article = Article(
                                        model = line.model,
                                        articleNumber = line.articleNumber,
                                        color = line.color,
                                        size = line.size,
                                    ),
                                    unitPrice = line.unitPrice,
                                    quantity = line.quantity,
                                )
                            }
                        }
                    }
                )
            )
        } catch (e: DuplicatedInvoiceId) {
            throw Conflict(e.message)
        } catch (e: DuplicatedArticleIdInInvoice) {
            throw BadRequest(e.message)
        } catch (e: ArticleNotFound) {
            throw BadRequest(e.message)
        } catch (e: ArticleWithNegativePrice) {
            throw BadRequest(e.message)
        }
    }

    suspend fun getAll(): List<InvoiceMetadataHttpResponse> {
        val response = listInvoicesQuery.execute(Unit)

        return response.map { invoiceMetadata ->
            InvoiceMetadataHttpResponse(
                id = invoiceMetadata.id,
                internalReference = invoiceMetadata.internalReference,
                externalReference = invoiceMetadata.externalReference,
                invoiceDate = invoiceMetadata.invoiceDate,
                amount = invoiceMetadata.amount
            )
        }
    }
}
