package be.boutiquemadame.elixir.gateway.adapters.contexts.real

import be.boutiquemadame.elixir.catalog.usecases.CreateArticleCommand
import be.boutiquemadame.elixir.catalog.usecases.CreateArticleRequest
import be.boutiquemadame.elixir.gateway.usecases.contracts.WarehouseContextGateway

class RealWarehouseContextGateway(
    val createProductCommand: CreateArticleCommand
) : WarehouseContextGateway {
    override suspend fun createArticle(request: CreateArticleRequest) {
        createProductCommand.execute(request)
    }
}
