import { Invoice, InvoiceRepository } from '../../../../core/contracts/invoice-repository';
import { never, Observable } from 'rxjs';
import { InvoiceMetadata } from '../../../../core/entities/invoice-metadata';
import { ajax } from 'rxjs/ajax';

export class InvoiceHttpRepository implements InvoiceRepository {
  public constructor(private API_ROOT: string) {}

  createInvoice(invoice: Invoice): Observable<void> {
    return ajax({
      method: 'POST',
      url: `${this.API_ROOT}/invoices`,
      headers: {
        'Content-Type': 'application/json',
      },
      body: {
        id: invoice.id,
        internalReference: invoice.internalReference,
        externalReference: invoice.externalReference,
        vatAmount: invoice.amount.toFixed(),
        invoiceDate: invoice.invoiceDate.toISODate(),
      },
    }).pipe(never);
  }

  getInvoiceMetadata(): Observable<InvoiceMetadata[]> {
    return ajax.getJSON<InvoiceMetadata[]>(`${this.API_ROOT}/invoices`);
  }
}
