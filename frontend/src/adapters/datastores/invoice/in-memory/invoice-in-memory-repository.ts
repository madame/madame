import { Invoice, InvoiceRepository } from '../../../../core/contracts/invoice-repository';
import { Observable, of } from 'rxjs';
import { InvoiceMetadata } from '../../../../core/entities/invoice-metadata';

export class InvoiceInMemoryRepository implements InvoiceRepository {
  private readonly invoices: InvoiceMetadata[] = [];

  createInvoice(invoice: Invoice): Observable<void> {
    this.invoices.push(
      new InvoiceMetadata({
        internalReference: invoice.internalReference,
        externalReference: invoice.externalReference,
        invoiceDate: invoice.invoiceDate.toISODate(),
        amount: invoice.amount.toString(),
        id: invoice.id,
      }),
    );
    return of();
  }

  getInvoiceMetadata(): Observable<InvoiceMetadata[]> {
    return of(this.invoices);
  }
}
