import { Container } from '../../core/contracts/container';
import { InvoiceInMemoryRepository } from '../datastores/invoice/in-memory/invoice-in-memory-repository';

export async function buildDevelopmentContainer(): Promise<Container> {
  return {
    debug: true,
    invoiceRepository: new InvoiceInMemoryRepository(),
  };
}
