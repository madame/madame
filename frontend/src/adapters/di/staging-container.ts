import { Container } from '../../core/contracts/container';
import { InvoiceHttpRepository } from '../datastores/invoice/http/invoice-http-repository';

export async function buildStagingContainer(): Promise<Container> {
  return {
    debug: false,
    invoiceRepository: new InvoiceHttpRepository(process.env.REACT_APP_API_ROOT as string),
  };
}
