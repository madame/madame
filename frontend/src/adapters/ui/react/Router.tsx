import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import ListInvoicesView from './views/list-invoices/ListInvoicesView';
import CreateInvoiceView from './views/create-invoice/CreateInvoiceView';

const Router: React.FC = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/invoices" component={ListInvoicesView} />
        <Route exact path="/invoices/create" component={CreateInvoiceView} />
        <Redirect to="/invoices" />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
