import React, { PropsWithChildren, ReactElement, useContext } from 'react';
import { Box, FormControl, Grid, GridProps, Paper, TextField, Typography } from '@material-ui/core';
import { TextFieldProps } from '@material-ui/core/TextField/TextField';
import { Autocomplete, AutocompleteProps } from '@material-ui/lab';
import { KeyboardDatePicker, KeyboardDatePickerProps } from '@material-ui/pickers';

export type ItemsPerLine = 'one' | 'three' | 'four';

const FormBlockContext = React.createContext<ItemsPerLine>('three');

export function FormBlock(props: PropsWithChildren<FormBlockProps>): ReactElement {
  return (
    <FormBlockContext.Provider value={props.itemsPerLine || 'three'}>
      <Paper>
        <Box m={1} p={1}>
          <Typography variant="h6">{props.title}</Typography>
          <Box mx={-2}>
            <Grid container alignItems="flex-start" justify="center">
              {props.children}
            </Grid>
          </Box>
        </Box>
      </Paper>
    </FormBlockContext.Provider>
  );
}

interface FormBlockProps {
  title: string;
  itemsPerLine?: ItemsPerLine;
}

type PropsPerItemsPerLine = {
  [P in ItemsPerLine]: GridProps;
};

export interface GridAlignerProps {
  itemsPerLine?: ItemsPerLine;
}

function GridAligner({
  children,
  itemsPerLine,
}: PropsWithChildren<GridAlignerProps>): ReactElement {
  const props: PropsPerItemsPerLine = {
    one: {
      xs: 12,
    },
    three: {
      xs: 12,
      md: 4,
    },
    four: {
      xs: 12,
      sm: 6,
      lg: 3,
    },
  };

  const defaultItemsPerLine = useContext(FormBlockContext);

  return (
    <Grid item {...props[itemsPerLine || defaultItemsPerLine]}>
      {children}
    </Grid>
  );
}

export function SpacedTextField({
  itemsPerLine,
  ...other
}: PropsWithChildren<TextFieldProps & GridAlignerProps>): ReactElement {
  return (
    <GridAligner itemsPerLine={itemsPerLine}>
      <Box mx={2}>
        <TextField fullWidth margin="normal" {...other} />
      </Box>
    </GridAligner>
  );
}

export function SpacedAutocomplete<
  T,
  U extends boolean | undefined,
  V extends boolean | undefined,
  W extends boolean | undefined
>({
  itemsPerLine,
  ...other
}: PropsWithChildren<AutocompleteProps<T, U, V, W> & GridAlignerProps>): ReactElement {
  return (
    <GridAligner itemsPerLine={itemsPerLine}>
      <Box mx={2}>
        <FormControl fullWidth margin="normal">
          <Autocomplete {...other} />
        </FormControl>
      </Box>
    </GridAligner>
  );
}

export function SpacedKeyboardDatePicker({
  itemsPerLine,
  ...other
}: PropsWithChildren<KeyboardDatePickerProps & GridAlignerProps>): ReactElement {
  return (
    <GridAligner itemsPerLine={itemsPerLine}>
      <Box mx={2}>
        <KeyboardDatePicker fullWidth margin="normal" {...other} />
      </Box>
    </GridAligner>
  );
}

export function SpacedRadioGroupWrapper({
  itemsPerLine,
  children,
}: PropsWithChildren<GridAlignerProps>): ReactElement {
  return (
    <GridAligner itemsPerLine={itemsPerLine}>
      <Box mx={2}>
        <FormControl fullWidth component="fieldset">
          {children}
        </FormControl>
      </Box>
    </GridAligner>
  );
}
