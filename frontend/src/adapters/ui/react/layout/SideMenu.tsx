import {
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
} from '@material-ui/core';
import { Dashboard, ListAlt, Receipt } from '@material-ui/icons';
import React from 'react';
import { createStyles, makeStyles } from '@material-ui/styles';

const drawerWidth = 240;

interface SideMenuProps {
  open: boolean;
}

const useStyles = makeStyles(() =>
  createStyles({
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    drawerContainer: {
      overflow: 'auto',
    },
  }),
);

const SideMenu: React.FC<SideMenuProps> = ({ open }) => {
  const classes = useStyles();

  return (
    <Drawer
      variant="persistent"
      open={open}
      className={classes.drawer}
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <Toolbar />
      <div className={classes.drawerContainer}>
        <List>
          <ListItem button>
            <ListItemIcon>
              <Dashboard />
            </ListItemIcon>
            <ListItemText primary="Dashboard" />
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem button>
            <ListItemIcon>
              <ListAlt />
            </ListItemIcon>
            <ListItemText primary="Invoices" />
          </ListItem>
        </List>
        <Divider />
        <List>
          <ListItem button>
            <ListItemIcon>
              <Receipt />
            </ListItemIcon>
            <ListItemText primary="Receipt" />
          </ListItem>
        </List>
      </div>
    </Drawer>
  );
};

export default SideMenu;
