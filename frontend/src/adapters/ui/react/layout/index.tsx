import React, { FC, useCallback } from 'react';
import Bar from './Bar';
import { Toolbar, Theme } from '@material-ui/core';
import { createStyles, makeStyles } from '@material-ui/styles';
import SideMenu from './SideMenu';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'column',
    },
    content: {
      flexGrow: 1,
      marginBottom: theme.spacing(3),
      marginTop: theme.spacing(3),
    },
  }),
);

const Layout: FC = ({ children }) => {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const toggleDrawer = useCallback(() => {
    setOpen(!open);
  }, [open]);

  return (
    <div className={classes.root}>
      <Bar onClick={toggleDrawer} />
      <SideMenu open={open} />
      <main className={classes.content}>
        <Toolbar />
        {children}
      </main>
    </div>
  );
};

export default Layout;
