import React, { FC } from 'react';
import { Typography, AppBar, Toolbar, IconButton, Theme } from '@material-ui/core';
import { AccountCircle, Menu } from '@material-ui/icons';
import { createStyles, makeStyles } from '@material-ui/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      zIndex: theme.zIndex.drawer + 1,
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    title: {
      flexGrow: 1,
    },
  }),
);

interface BarProps {
  onClick: () => void;
}

const Bar: FC<BarProps> = ({ onClick }) => {
  const classes = useStyles();

  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar>
        <IconButton
          edge="start"
          color="inherit"
          aria-label="menu"
          className={classes.menuButton}
          onClick={onClick}
        >
          <Menu />
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          Madame
        </Typography>
        <IconButton color="inherit">
          <AccountCircle />
        </IconButton>
      </Toolbar>
    </AppBar>
  );
};

export default Bar;
