import { createMuiTheme } from '@material-ui/core';
import { deepPurple, pink } from '@material-ui/core/colors';

export default createMuiTheme({
  palette: {
    primary: deepPurple,
    secondary: pink,
  },
  props: {
    MuiTextField: {
      variant: 'outlined',
    },
  },
});
