import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { Container } from '../../../core/contracts/container';

export function startReact(container: Container): void {
  ReactDOM.render(<App container={container} />, document.getElementById('root'));
}
