import React from 'react';
import { CssBaseline } from '@material-ui/core';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import { ThemeProvider } from '@material-ui/styles';
import theme from './theme';
import DateUtils from '@date-io/luxon';
import { Provider } from 'react-redux';
import { Container } from '../../../core/contracts/container';
import { createStore } from '../../../core/store';
import Router from './Router';
import Layout from './layout';

interface AppProps {
  container: Container;
}

const App: React.FC<AppProps> = ({ container }) => {
  const store = createStore(container);

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <MuiPickersUtilsProvider utils={DateUtils}>
          <CssBaseline />
          <Layout>
            <Router />
          </Layout>
        </MuiPickersUtilsProvider>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
