import React, { ReactElement, useLayoutEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../../../core/use-cases';
import { requestInvoiceListLoading } from '../../../../../core/use-cases/list-invoices';
import InvoicesTable from './InvoiceTable';

const ListInvoicesView: React.FC = function (): ReactElement {
  const state = useSelector((state: RootState) => state.listInvoices);
  const dispatch = useDispatch();

  useLayoutEffect(() => {
    dispatch(requestInvoiceListLoading());
  }, [dispatch]);

  if (state.status === 'LOADING' || state.status === 'ERROR') return <span>state.status</span>;

  return <InvoicesTable invoices={state.invoices} />;
};

export default ListInvoicesView;
