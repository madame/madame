import React, { ReactElement } from 'react';
import { InvoiceMetadataState } from '../../../../../core/entities/invoice-metadata';
import {
  Table,
  Paper,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Button,
  Container,
} from '@material-ui/core';
import { Link } from 'react-router-dom';

interface InvoiceTableProps {
  invoices: InvoiceMetadataState[];
}

const InvoiceTable: React.FC<InvoiceTableProps> = function ({ invoices }): ReactElement {
  return (
    <Container maxWidth={false}>
      <Button component={Link} to="/invoices/create" variant="contained" color="primary">
        Add Invoice
      </Button>
      <Paper>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Internal Reference</TableCell>
              <TableCell>External Reference</TableCell>
              <TableCell>Date</TableCell>
              <TableCell>Amount</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {invoices.map((invoice) => (
              <TableRow key={invoice.id}>
                <TableCell>{invoice.internalReference}</TableCell>
                <TableCell>{invoice.externalReference}</TableCell>
                <TableCell>{invoice.invoiceDate}</TableCell>
                <TableCell align="right">{invoice.amount} €</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </Container>
  );
};

export default InvoiceTable;
