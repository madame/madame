import React, { ReactElement } from 'react';
import Form from './Form';
import { useDispatch } from 'react-redux';
import { submitInvoiceCreation } from '../../../../../core/use-cases/create-invoice/actions';
import { DateTime } from 'luxon';

const CreateInvoiceView: React.FC = function (): ReactElement {
  const dispatch = useDispatch();

  function submit() {
    dispatch(
      submitInvoiceCreation('internalReference', 'externalReference', DateTime.local(), '123.45'),
    );
  }

  return <Form submit={submit} />;
};

export default CreateInvoiceView;
