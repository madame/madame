import React, { ReactElement } from 'react';
import {
  Breadcrumbs,
  Button,
  Container,
  FormControlLabel,
  FormLabel,
  Paper,
  Radio,
  RadioGroup,
  Table,
  TableCell,
  TableContainer,
  TableHead,
  TextField,
  Typography,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import {
  SpacedTextField,
  SpacedAutocomplete,
  FormBlock,
  SpacedRadioGroupWrapper,
} from '../../components/Form';
import MetadataEditableForm from './parts/MetadataEditableForm';

interface FormProps {
  submit: () => void;
}

const Form: React.FC<FormProps> = function ({ submit }): ReactElement {
  return (
    <Container maxWidth="lg">
      <Typography variant="h4">Add Invoice</Typography>
      <Breadcrumbs>
        <Link to="/">Dashboard</Link>
        <Link to="/invoices">Invoices</Link>
        <span>Add</span>
      </Breadcrumbs>
      <MetadataEditableForm />
      <FormBlock title="Line Type">
        <SpacedRadioGroupWrapper itemsPerLine="one">
          <FormLabel component="legend" hidden>
            Line Type
          </FormLabel>
          <RadioGroup aria-label="gender" name="type" value="no_articles">
            <FormControlLabel value="plain" control={<Radio />} label="Plain text line" />
            <FormControlLabel value="no_articles" control={<Radio />} label="Article" />
          </RadioGroup>
        </SpacedRadioGroupWrapper>
      </FormBlock>
      <FormBlock title="Add Plain Text Line" itemsPerLine="four">
        <SpacedTextField label="Description" />
        <SpacedTextField label="Quantity" inputMode="numeric" />
        <SpacedTextField label="Unit Price" inputMode="numeric" />
        <SpacedTextField disabled label="Total Price" inputMode="numeric" />
      </FormBlock>
      <FormBlock title="Add Article Line" itemsPerLine="four">
        <SpacedAutocomplete
          renderInput={(params) => <TextField {...params} label="Model" />}
          options={['Model 1', 'Model 2', 'Model 3']}
          freeSolo
        />
        <SpacedAutocomplete
          renderInput={(params) => <TextField {...params} label="Article Number" />}
          options={['AZ-14 DE', 'LED14ED', '1457EO']}
          freeSolo
        />
        <SpacedAutocomplete
          renderInput={(params) => <TextField {...params} label="Size" />}
          options={['36', '38', '40']}
          freeSolo
        />
        <SpacedAutocomplete
          renderInput={(params) => <TextField {...params} label="Color" />}
          options={['Red', 'Green', 'Blue']}
          freeSolo
        />
        <SpacedTextField label="Quantity" inputMode="numeric" />
        <SpacedTextField label="Unit Price" inputMode="numeric" />
        <SpacedTextField disabled label="Total Price" inputMode="numeric" />
        <SpacedTextField label="Selling Price" inputMode="numeric" />
      </FormBlock>
      <Button variant="contained" color="primary" fullWidth>
        Add Line
      </Button>
      <TableContainer component={Paper}>
        <Table>
          <TableHead>
            <TableCell>Model</TableCell>
            <TableCell>Article Number</TableCell>
            <TableCell>Size</TableCell>
            <TableCell>Color</TableCell>
            <TableCell>Quantity</TableCell>
            <TableCell>Unit Price</TableCell>
            <TableCell>Total Price</TableCell>
            <TableCell>Selling Price</TableCell>
            <TableCell>Remarks</TableCell>
          </TableHead>
        </Table>
      </TableContainer>
      <FormBlock title="Summary">
        <SpacedTextField disabled label="Sum of all articles" inputMode="numeric" />
        <SpacedTextField disabled label="Invoice total" inputMode="numeric" />
        <SpacedTextField disabled label="Difference" inputMode="numeric" />
      </FormBlock>
      <Button variant="contained" color="primary" fullWidth onClick={submit}>
        Submit
      </Button>
    </Container>
  );
};

export default Form;
