import { FormControlLabel, FormLabel, Radio, RadioGroup, TextField } from '@material-ui/core';
import {
  FormBlock,
  SpacedAutocomplete,
  SpacedKeyboardDatePicker,
  SpacedRadioGroupWrapper,
  SpacedTextField,
} from '../../../components/Form';
import { DateTime } from 'luxon';
import React, { ReactElement } from 'react';

export type MetadataFormDisplayProps = MetadataFormDisplayMandatoryProps &
  (MetadataFormDisplayReadOnlyProps | MetadataFormDisplayEditableProps);

interface MetadataFormDisplayMandatoryProps {
  externalReference: string;
  internalReference: string;
  invoiceDate: DateTime | null;
  supplier: string | null;
  amount: string;
  vatAmount: string;
  invoiceType: string;
}

interface MetadataFormDisplayReadOnlyProps {
  disabled: true;
}

interface MetadataFormDisplayEditableProps {
  disabled: false;
  setExternalReference: (externalReference: string) => void;
  blurExternalReference?: () => void;
  externalReferenceError?: string;
  setInternalReference: (internalReference: string) => void;
  blurInternalReference?: () => void;
  internalReferenceError?: string;
  setInvoiceDate: (invoiceDate: DateTime | null) => void;
  blurInvoiceDate?: () => void;
  invoiceDateError?: string;
  setSupplier: (supplier: string | null) => void;
  blurSupplier?: () => void;
  supplierError?: string;
  setAmount: (amount: string) => void;
  blurAmount?: () => void;
  amountError?: string;
  setVatAmount: (vatAmount: string) => void;
  blurVatAmount?: () => void;
  vatAmountError?: string;
  setInvoiceType: (invoiceType: string) => void;
  blurInvoiceType?: () => void;
  invoiceTypeError?: string;
}

export default function MetadataFormContent(props: MetadataFormDisplayProps): ReactElement {
  return (
    <>
      <FormBlock title="General Information">
        <SpacedTextField
          label="Internal reference"
          value={props.internalReference}
          onChange={(e) => {
            !props.disabled && props.setInternalReference(e.target.value);
          }}
          error={!props.disabled && !!props.internalReferenceError}
          helperText={!props.disabled && props.internalReferenceError}
          disabled={props.disabled}
          onBlur={() => {
            !props.disabled && props.blurInternalReference && props.blurInternalReference();
          }}
        />
        <SpacedTextField
          label="External reference"
          value={props.externalReference}
          onChange={(e) => {
            !props.disabled && props.setExternalReference(e.target.value);
          }}
          error={!props.disabled && !!props.externalReferenceError}
          helperText={!props.disabled && props.externalReferenceError}
          disabled={props.disabled}
          onBlur={() => {
            !props.disabled && props.blurExternalReference && props.blurExternalReference();
          }}
        />
        <SpacedKeyboardDatePicker
          label="Invoice date"
          format="yyyy-MM-dd"
          value={props.invoiceDate}
          onChange={(date) => {
            !props.disabled && props.setInvoiceDate(date as DateTime);
          }}
          error={!props.disabled && !!props.invoiceDateError}
          helperText={!props.disabled && props.invoiceDateError}
          disabled={props.disabled}
          onBlur={() => {
            !props.disabled && props.blurInvoiceDate && props.blurInvoiceDate();
          }}
        />
        <SpacedAutocomplete
          itemsPerLine="one"
          options={['Supplier 1', 'Supplier 2', 'Supplier 3']}
          value={props.supplier}
          onChange={(e: unknown, value: string | null | string[]) => {
            if (Array.isArray(value)) return;
            !props.disabled && props.setSupplier(value);
          }}
          renderInput={(params) => (
            <TextField
              {...params}
              label="Supplier"
              variant="outlined"
              error={!props.disabled && !!props.supplierError}
              helperText={!props.disabled && props.supplierError}
            />
          )}
          disabled={props.disabled}
          onBlur={() => {
            !props.disabled && props.blurSupplier && props.blurSupplier();
          }}
        />
      </FormBlock>
      <FormBlock title="Amounts">
        <SpacedTextField
          label="Amount without VAT"
          value={props.amount}
          inputMode="numeric"
          onChange={(e) => {
            !props.disabled && props.setAmount(e.target.value);
          }}
          error={!props.disabled && !!props.amountError}
          helperText={!props.disabled && props.amountError}
          disabled={props.disabled}
          onBlur={() => {
            !props.disabled && props.blurAmount && props.blurAmount();
          }}
        />
        <SpacedTextField
          label="VAT Amount"
          value={props.vatAmount}
          inputMode="numeric"
          onChange={(e) => {
            !props.disabled && props.setVatAmount(e.target.value);
          }}
          error={!props.disabled && !!props.vatAmountError}
          helperText={!props.disabled && props.vatAmountError}
          disabled={props.disabled}
          onBlur={() => {
            !props.disabled && props.blurVatAmount && props.blurVatAmount();
          }}
        />
        <SpacedTextField
          label="Total Amount"
          value={(Number(props.amount) || 0) + (Number(props.vatAmount) || 0)}
          inputMode="numeric"
          disabled
        />
      </FormBlock>
      <FormBlock title="Invoice Type">
        <SpacedRadioGroupWrapper itemsPerLine="one">
          <FormLabel component="legend" hidden>
            Invoice Type
          </FormLabel>
          <RadioGroup
            aria-label="invoice-type"
            name="type"
            value={props.invoiceType}
            onChange={(e) => {
              !props.disabled && props.setInvoiceType(e.target.value);
            }}
            onBlur={() => {
              !props.disabled && props.blurInvoiceType && props.blurInvoiceType();
            }}
          >
            <FormControlLabel
              value="articles"
              control={<Radio />}
              label="I want to lines to this invoice"
              disabled={props.disabled}
            />
            <FormControlLabel
              value="no_articles"
              control={<Radio />}
              label="Just save it"
              disabled={props.disabled}
            />
          </RadioGroup>
        </SpacedRadioGroupWrapper>
      </FormBlock>
    </>
  );
}
