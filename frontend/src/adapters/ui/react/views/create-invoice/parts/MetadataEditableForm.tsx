import { DateTime } from 'luxon';
import React, {
  Dispatch,
  ReactElement,
  SetStateAction,
  useCallback,
  useEffect,
  useState,
} from 'react';
import MetadataFormContent from './MetadataFormContent';
import { Button } from '@material-ui/core';

export interface MetadataEditableFormProps {
  internalReferenceError?: string;
  externalReferenceError?: string;
}

export default function MetadataEditableForm(props: MetadataEditableFormProps): ReactElement {
  const [internalReference, setInternalReference] = useState('');
  const [externalReference, setExternalReference] = useState('');
  const [invoiceDate, setInvoiceDate] = useState<DateTime | null>(null);
  const [supplier, setSupplier] = useState<string | null>(null);
  const [amount, setAmount] = useState<string>('');
  const [vatAmount, setVatAmount] = useState('');
  const [invoiceType, setInvoiceType] = useState('');
  const [internalReferenceError, setInternalReferenceError] = useState<string | undefined>(
    undefined,
  );
  const [externalReferenceError, setExternalReferenceError] = useState<string | undefined>(
    undefined,
  );
  const [invoiceDateError, setInvoiceDateError] = useState<string | undefined>(undefined);
  const [supplierError, setSupplierError] = useState<string | undefined>(undefined);
  const [amountError, setAmountError] = useState<string | undefined>(undefined);
  const [vatAmountError, setVatAmountError] = useState<string | undefined>(undefined);

  useEffect(() => {
    setInternalReferenceError(props.internalReferenceError);
    setExternalReferenceError(props.externalReferenceError);
  }, [props.internalReferenceError, props.externalReferenceError]);

  const validateNonEmpty = useCallback(
    (value: string, setError: Dispatch<SetStateAction<string | undefined>>): boolean => {
      const isValid = value !== '';
      setError(isValid ? undefined : 'This value should not be empty');
      return isValid;
    },
    [],
  );

  const validateNonNull = useCallback(
    <T,>(
      value: T,
      setError: Dispatch<SetStateAction<string | undefined>>,
    ): value is Exclude<T, null> => {
      const isValid = value !== null;
      setError(isValid ? undefined : 'This value should not be empty');
      return isValid;
    },
    [],
  );

  const validateDate = useCallback(
    (value: DateTime, setError: Dispatch<SetStateAction<string | undefined>>): boolean => {
      const isValid = value.isValid;
      setError(isValid ? undefined : 'Invalid date');
      return isValid;
    },
    [],
  );

  const validateNumber = useCallback(
    (value: string, setError: Dispatch<SetStateAction<string | undefined>>): boolean => {
      const numberRegex = /^[0-9]+(\.[0-9]+)?$/;
      const isValid = numberRegex.test(value);
      setError(isValid ? undefined : 'Invalid number');
      return isValid;
    },
    [],
  );

  const validateInternalReference = useCallback((): boolean => {
    const trimmed = internalReference.trim();
    setInternalReference(trimmed);
    return validateNonEmpty(trimmed, setInternalReferenceError);
  }, [internalReference]);

  const changeInternalReference = useCallback((value: string) => {
    setInternalReferenceError(undefined);
    setInternalReference(value);
  }, []);

  const validateExternalReference = useCallback((): boolean => {
    setExternalReference(externalReference.trim());
    return true;
  }, [externalReference]);

  const changeExternalReference = useCallback((value: string) => {
    setExternalReferenceError(undefined);
    setExternalReference(value);
  }, []);

  const validateInvoiceDate = useCallback((): boolean => {
    return (
      validateNonNull(invoiceDate, setInvoiceDateError) &&
      validateDate(invoiceDate, setInvoiceDateError)
    );
  }, [invoiceDate]);

  const changeInvoiceDate = useCallback((value: DateTime | null) => {
    setInvoiceDateError(undefined);
    setInvoiceDate(value);
  }, []);

  const validateSupplier = useCallback((): boolean => {
    return validateNonNull(supplier, setSupplierError);
  }, [supplier]);

  const changeSupplier = useCallback((value: string | null) => {
    setSupplierError(undefined);
    setSupplier(value);
  }, []);

  const validateAmount = useCallback((): boolean => {
    return validateNonEmpty(amount, setAmountError) && validateNumber(amount, setAmountError);
  }, [amount]);

  const changeAmount = useCallback((value: string) => {
    value = value?.replaceAll(',', '.').trim();
    setAmountError(undefined);
    setAmount(value);
  }, []);

  const validateVatAmount = useCallback((): boolean => {
    return (
      validateNonEmpty(vatAmount, setVatAmountError) && validateNumber(vatAmount, setVatAmountError)
    );
  }, [vatAmount]);

  const changeVatAmount = useCallback((value: string) => {
    value = value?.replaceAll(',', '.').trim();
    setVatAmountError(undefined);
    setVatAmount(value);
  }, []);

  const submit = useCallback(() => {
    let isFormValid = true;
    isFormValid = validateInternalReference() && isFormValid;
    isFormValid = validateInvoiceDate() && isFormValid;
    isFormValid = validateSupplier() && isFormValid;
    isFormValid = validateAmount() && isFormValid;
    isFormValid = validateVatAmount() && isFormValid;
    isFormValid = !!invoiceType && isFormValid;
    if (isFormValid) {
      alert('submit');
    }
  }, [
    validateInternalReference,
    validateInvoiceDate,
    validateSupplier,
    validateAmount,
    validateVatAmount,
    invoiceType,
  ]);

  const formValid =
    !internalReferenceError &&
    !invoiceDateError &&
    !supplierError &&
    !amountError &&
    !vatAmountError &&
    invoiceType;

  return (
    <>
      <MetadataFormContent
        disabled={false}
        internalReference={internalReference}
        setInternalReference={changeInternalReference}
        blurInternalReference={validateInternalReference}
        internalReferenceError={internalReferenceError}
        externalReference={externalReference}
        setExternalReference={changeExternalReference}
        blurExternalReference={validateExternalReference}
        externalReferenceError={externalReferenceError}
        invoiceDate={invoiceDate}
        setInvoiceDate={changeInvoiceDate}
        blurInvoiceDate={validateInvoiceDate}
        invoiceDateError={invoiceDateError}
        supplier={supplier}
        setSupplier={changeSupplier}
        blurSupplier={validateSupplier}
        supplierError={supplierError}
        amount={amount}
        setAmount={changeAmount}
        blurAmount={validateAmount}
        amountError={amountError}
        vatAmount={vatAmount}
        setVatAmount={changeVatAmount}
        blurVatAmount={validateVatAmount}
        vatAmountError={vatAmountError}
        invoiceType={invoiceType}
        setInvoiceType={setInvoiceType}
      />
      <Button variant="contained" color="primary" fullWidth disabled={!formValid} onClick={submit}>
        {invoiceType === 'articles' ? 'Continue with articles' : 'Save Invoice'}
      </Button>
    </>
  );
}
