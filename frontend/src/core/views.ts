export const CREATE_INVOICE_VIEW = 'CREATE_INVOICE_VIEW';

export interface CreateInvoiceView {
  type: typeof CREATE_INVOICE_VIEW;
}

export const VIEW_INVOICE_VIEW = 'VIEW_INVOICE_VIEW';

export interface ViewInvoiceView {
  type: typeof VIEW_INVOICE_VIEW;
  invoiceId: string;
}

export const LIST_INVOICE_VIEW = 'LIST_INVOICE_VIEW';

export interface ListInvoiceView {
  type: typeof LIST_INVOICE_VIEW;
}

export type View = CreateInvoiceView | ListInvoiceView | ViewInvoiceView;
