import BigNumber from 'bignumber.js';
import { DateTime } from 'luxon';

export interface InvoiceMetadataState {
  id: string;
  internalReference: string;
  externalReference: string;
  invoiceDate: string;
  amount: string;
}

export class InvoiceMetadata {
  public readonly id: string;
  public readonly internalReference: string;
  public readonly externalReference: string;
  public readonly invoiceDate: DateTime;
  public readonly amount: BigNumber;

  public constructor(props: InvoiceMetadataState) {
    this.id = props.id;
    this.internalReference = props.internalReference;
    this.externalReference = props.externalReference;
    this.invoiceDate = DateTime.fromISO(props.invoiceDate);
    this.amount = new BigNumber(props.amount);
  }

  public toState(): InvoiceMetadataState {
    return {
      id: this.id,
      internalReference: this.internalReference,
      externalReference: this.externalReference,
      invoiceDate: this.invoiceDate.toISODate(),
      amount: this.amount.toFixed(2),
    };
  }
}
