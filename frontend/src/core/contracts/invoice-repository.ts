import { Observable } from 'rxjs';
import { InvoiceMetadata } from '../entities/invoice-metadata';
import { DateTime } from 'luxon';
import BigNumber from 'bignumber.js';

export interface Invoice {
  id: string;
  internalReference: string;
  externalReference: string;
  amount: BigNumber;
  invoiceDate: DateTime;
}

export interface InvoiceRepository {
  createInvoice(invoice: Invoice): Observable<void>;
  getInvoiceMetadata(): Observable<InvoiceMetadata[]>;
}
