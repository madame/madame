import { InvoiceRepository } from './invoice-repository';

export interface Container {
  invoiceRepository: InvoiceRepository;
  debug: boolean;
}
