import { applyMiddleware, Store } from 'redux';
import { Container } from './contracts/container';
import { rootEpic, rootReducer } from './use-cases';
import { createStore as reduxCreateStore, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { createEpicMiddleware } from 'redux-observable';

export function createStore(container: Container): Store {
  const composeEnhancers = container.debug ? composeWithDevTools({}) : compose;

  const epicMiddleware = createEpicMiddleware({
    dependencies: container,
  });

  const store = reduxCreateStore(rootReducer, composeEnhancers(applyMiddleware(epicMiddleware)));

  epicMiddleware.run(rootEpic);

  return store;
}
