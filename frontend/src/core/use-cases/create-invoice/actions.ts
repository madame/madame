import { DateTime } from 'luxon';
import BigNumber from 'bignumber.js';
import { v4 as uuid } from 'uuid';

export const INVOICE_CREATE_SUBMIT = 'INVOICE_CREATE_SUBMIT';

export interface SubmitInvoiceCreationAction {
  type: typeof INVOICE_CREATE_SUBMIT;
  id: string;
  internalReference: string;
  externalReference: string;
  invoiceDate: DateTime;
  amount: BigNumber;
}

export type CreateInvoiceActionsTypes = SubmitInvoiceCreationAction;

export function submitInvoiceCreation(
  internalReference: string,
  externalReference: string,
  invoiceDate: DateTime,
  amount: string,
): CreateInvoiceActionsTypes {
  return {
    type: INVOICE_CREATE_SUBMIT,
    id: uuid(),
    internalReference,
    externalReference,
    invoiceDate,
    amount: new BigNumber(amount),
  };
}
