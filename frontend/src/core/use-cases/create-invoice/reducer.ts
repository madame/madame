import { Epic, ofType } from 'redux-observable';
import { Observable, of } from 'rxjs';
import { RootAction, RootState } from '../index';
import { Container } from '../../contracts/container';
import { INVOICE_CREATE_SUBMIT, SubmitInvoiceCreationAction } from './actions';
import { catchError, switchMap } from 'rxjs/operators';

export const createInvoiceEpic: Epic = (
  $action: Observable<RootAction>,
  $state: Observable<RootState>,
  { invoiceRepository }: Container,
) =>
  $action.pipe(
    ofType(INVOICE_CREATE_SUBMIT),
    switchMap((action) => {
      console.log('network');
      const invoice = action as SubmitInvoiceCreationAction;
      return invoiceRepository.createInvoice(invoice).pipe(
        catchError((e) => {
          console.log(e);
          return of();
        }),
      );
    }),
  );
