import { combineReducers } from 'redux';
import { combineEpics } from 'redux-observable';
import { listInvoiceEpic, ListInvoicesActionsTypes, listInvoicesReducer } from './list-invoices';
import { CreateInvoiceActionsTypes } from './create-invoice/actions';
import { createInvoiceEpic } from './create-invoice/reducer';

export const rootReducer = combineReducers({
  listInvoices: listInvoicesReducer,
});

export type RootAction = ListInvoicesActionsTypes | CreateInvoiceActionsTypes;

export type RootState = ReturnType<typeof rootReducer>;

export const rootEpic = combineEpics(listInvoiceEpic, createInvoiceEpic);
