import { Observable, of } from 'rxjs';
import { Container } from '../contracts/container';
import { Epic, ofType } from 'redux-observable';
import { catchError, map, switchMap } from 'rxjs/operators';
import { InvoiceMetadata, InvoiceMetadataState } from '../entities/invoice-metadata';
import { RootAction, RootState } from './index';

export const LOAD_INVOICE_LIST_REQUESTED = 'LOAD_INVOICE_LIST_REQUESTED';
export const LOAD_INVOICE_LIST_SUCCEEDED = 'LOAD_INVOICE_LIST_SUCCEEDED';
export const LOAD_INVOICE_LIST_FAILED = 'LOAD_INVOICE_LIST_FAILED';

interface LoadInvoiceListRequestedAction {
  type: typeof LOAD_INVOICE_LIST_REQUESTED;
}

interface LoadInvoiceListSucceededAction {
  type: typeof LOAD_INVOICE_LIST_SUCCEEDED;
  invoices: InvoiceMetadataState[];
}

interface LoadInvoiceListFailedAction {
  type: typeof LOAD_INVOICE_LIST_FAILED;
}

export type ListInvoicesActionsTypes =
  | LoadInvoiceListRequestedAction
  | LoadInvoiceListSucceededAction
  | LoadInvoiceListFailedAction;

export function requestInvoiceListLoading(): LoadInvoiceListRequestedAction {
  return {
    type: LOAD_INVOICE_LIST_REQUESTED,
  };
}

function requestInvoiceListSucceeded(invoices: InvoiceMetadata[]): LoadInvoiceListSucceededAction {
  return {
    type: LOAD_INVOICE_LIST_SUCCEEDED,
    invoices: invoices.map((i) => i.toState()),
  };
}

function requestInvoiceListFailed(): LoadInvoiceListFailedAction {
  return {
    type: LOAD_INVOICE_LIST_FAILED,
  };
}

interface LoadingState {
  status: 'LOADING';
}

interface LoadedState {
  status: 'LOADED';
  invoices: InvoiceMetadataState[];
}

interface ErrorState {
  status: 'ERROR';
}

export type ListInvoicesState = LoadingState | LoadedState | ErrorState;

const initialState: ListInvoicesState = {
  status: 'ERROR',
};

export function listInvoicesReducer(
  state = initialState,
  action: ListInvoicesActionsTypes,
): ListInvoicesState {
  switch (action.type) {
    case 'LOAD_INVOICE_LIST_REQUESTED': {
      return {
        status: 'LOADING',
      };
    }
    case 'LOAD_INVOICE_LIST_SUCCEEDED': {
      return {
        status: 'LOADED',
        invoices: action.invoices,
      };
    }
    case 'LOAD_INVOICE_LIST_FAILED': {
      return {
        status: 'ERROR',
      };
    }
    default:
      return state;
  }
}

export const listInvoiceEpic: Epic = (
  action$: Observable<RootAction>,
  _state$: Observable<RootState>,
  { invoiceRepository }: Container,
) =>
  action$.pipe(
    ofType(LOAD_INVOICE_LIST_REQUESTED),
    switchMap(() =>
      invoiceRepository.getInvoiceMetadata().pipe(
        map(requestInvoiceListSucceeded),
        catchError(() => {
          return of(requestInvoiceListFailed());
        }),
      ),
    ),
  );
