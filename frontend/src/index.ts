import { startReact } from './adapters/ui/react';
import * as serviceWorker from './serviceWorker';
import { buildDevelopmentContainer } from './adapters/di/development-container';
import { buildStagingContainer } from './adapters/di/staging-container';

(async function (): Promise<void> {
  const container =
    process.env.NODE_ENV === 'production'
      ? await buildStagingContainer()
      : await buildDevelopmentContainer();

  startReact(container);
})();

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
